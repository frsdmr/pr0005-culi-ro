package ed.str.culiro;

import ed.str.culiro.food.Recipe;
import ed.str.culiro.food.RecipeStep;
import ed.str.culiro.food.RecipesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;


@SpringBootApplication

@Controller

public class MainAppStarter {


    @Autowired
    public MainAppStarter(RecipesRepo recipeSRepo) {
        this.recipeSRepo = recipeSRepo;
    }

    @GetMapping("/")
    public String index(Principal user, HttpServletRequest request, Authentication authentication,
                       @RequestParam(name="nam", required=false, defaultValue="some") String namex, @RequestParam(name="sec", required=false ) String sec, /*Model model*/ Map<String, Object> model) {
//        model.addAttribute("name", namex);
//        model.addAttribute("sec", sec);
        model.put("authentication", authentication);
        model.put("shn", "the shn here");
        System.out.println("\n ***   index !!!!!!!!! ");
        System.out.println(user);
        System.out.println(request);
        System.out.println(authentication);
        return "index.jsp";
    }

    private final RecipesRepo recipeSRepo;

    @GetMapping("/aaa")
    public String aaa(Principal user, HttpServletRequest request, Authentication authentication,
                       @RequestParam(name="nam", required=false, defaultValue="some") String namex, @RequestParam(name="sec", required=false ) String sec, Model model) {
        System.out.println("***");
        Recipe r = new Recipe();
        r.setName("[name "+new Date()+"]");
        r.setRecipeSteps(new ArrayList<>());
        recipeSRepo.save(r);

        RecipeStep s1 = new RecipeStep();
        s1.setName("name 1777");
        s1.setParentRecipe(r);

        RecipeStep s2 = new RecipeStep();
        s2.setName("SHN 2777");
        s2.setDescription("desk here");
        s2.setParentRecipe(r);

        r.getRecipeSteps().add(s1);
        r.getRecipeSteps().add(s2);

        recipeSRepo.save(r);

        System.out.println("***");
        //System.out.println(r);
        return "index.jsp";
    }

    public static void main(String[] args) {
        SpringApplication.run(MainAppStarter.class, args);
    }
}
