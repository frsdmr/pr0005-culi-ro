package ed.str.culiro.food;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController

public class RecipeDBController {

    @Autowired
    private RecipesRepo recipeSRepo;

    @GetMapping("/rcp/{recipeId}")
    public Recipe recipeById(@PathVariable(name="recipeId",required=false) String recipeId ) {
        Recipe recipe;
        recipe = recipeSRepo.findById(Long.parseLong(recipeId)).orElse(new Recipe("SHN ["+recipeId+"]"));
        return recipe;
    }

    @GetMapping("/findByShortNameIgnoreCaseLike/{shortName}")
    public List<Recipe> findByShortNameIgnoreCaseLike(@PathVariable(name="shortName",required=false) String shortName ) {
        ArrayList recipes = null;
        recipes = (ArrayList) recipeSRepo.findByShortNameIgnoreCaseLike( "%"+shortName + "%" );//.  //orElse(new Recipe("SHN ["+shortName+"]"));
        return recipes;
    }

    @GetMapping("/findByShortNameLikeIgnoreCase/{shortName}")
    public List<Recipe> findByShortNameLikeIgnoreCase(@PathVariable(name="shortName",required=false) String shortName ) {
        ArrayList recipes = null;
        recipes = (ArrayList) recipeSRepo.findByShortNameLikeIgnoreCase( "%"+shortName + "%" );//.  //orElse(new Recipe("SHN ["+shortName+"]"));
        return recipes;
    }
}
