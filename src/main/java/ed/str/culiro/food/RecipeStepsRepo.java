package ed.str.culiro.food;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeStepsRepo extends PagingAndSortingRepository<RecipeStep, Long> {
}
