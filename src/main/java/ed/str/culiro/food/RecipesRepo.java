package ed.str.culiro.food;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


//(https://www.baeldung.com/spring-data-annotations)
//(https://docs.spring.io/spring-data/jpa/docs/current/reference/html/)
//(http://javastudy.ru/spring-data-jpa/annotation-persistence/)



//@RepositoryRestResource(collectionResourceRel = "foodRecipes", path = "fRecipes")
public interface RecipesRepo extends PagingAndSortingRepository<Recipe, Long> {

    List<Recipe> findByTts(@Param("tts") String tts);

    List<Recipe> findByShortNameIgnoreCaseLike (@Param("snicl") String shortName);

    List<Recipe> findByShortNameLikeIgnoreCase (@Param("snlic") String shortName);
}
