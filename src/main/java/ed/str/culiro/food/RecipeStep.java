package ed.str.culiro.food;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;


@Entity
@Table( name = "food_recipe_steps")
@Data

public class RecipeStep {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long recipeStepId;


    @Transient
    private String a_The_Step_Recipe = "";


    private String name;


    private String description;


    @ManyToOne( fetch = FetchType.EAGER )           //
    @JoinColumn( name = "parentRecipeIdLink" )
    @JsonIgnore                                                             // IGNORE -->> not send to JSON
    private Recipe parentRecipe;


    @Transient                                                              // IGNORE -->> not write to DB
    private String parentRecipeId;
    private String getParentRecipeId() {
        return (parentRecipe==null ? "" : ""+parentRecipe.getRecipeId() );
    }


    @Override
    public String toString() {
        return "RecipeStep{" +
                "recipeStepId=" + recipeStepId +
                ", a_The_Step_Recipe='" + a_The_Step_Recipe + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", parentRecipeId='" + getParentRecipeId() + '\'' +
                '}';
    }
}
