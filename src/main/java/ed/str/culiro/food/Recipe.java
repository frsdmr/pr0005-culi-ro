package ed.str.culiro.food;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "food_recipes")
@Data

public class Recipe {

    public Recipe(){
    }

    @Id
    @GeneratedValue
    private Long recipeId;

    @Transient
    private String a_The_Recipe = "";


    private String shortName;


    private String name;


    private String tts;


    @Transient
    private Integer recipeStepsCount;
    public Integer getRecipeStepsCount() {
        return recipeSteps.size();
    }


    @OneToMany(mappedBy = "parentRecipe",  // use it when bi-directional (field name ("parentRecipe") in child base)
            fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval=true)
    public List<RecipeStep> recipeSteps = new ArrayList();


    Recipe(String shn) {
        shortName = shn;
    }
}
