package ed.str.culiro;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


@Controller
@EnableWebSecurity
public class BlockSecurity extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/api/**","/yandex_4f2d534486f17935.html","/theSomeAbout","/gm","/js/lib/*","/geodata/*").permitAll();

        http.authorizeRequests()
                .antMatchers("/gru0").access("hasAnyRole('USER','ADM')")
                .antMatchers("/gru1").hasAnyRole("USER","ADM")
                .antMatchers("/gra","/actuator/shutdown").hasRole("ADM")
                .anyRequest().authenticated();

        http.csrf().disable();

        http.antMatcher("/**")
                .logout().logoutSuccessUrl("/login").permitAll();

        http                                                                // >>>>>   for password block auth start
                .formLogin()
                .loginPage("/login")                                        // to auth form
                .permitAll();                                               // <<<<<<  for password block auth finish


    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)	throws Exception {
        auth.inMemoryAuthentication().withUser("u").password("{noop}p").roles("USER");
        auth.inMemoryAuthentication().withUser("a").password("{noop}p").roles("ADM");
        auth.inMemoryAuthentication().withUser("m").password("{noop}p").roles("DEV","MGROUP");
    }

    @GetMapping("/login")               // when  password auth
    public String login(Authentication authentication, Map<String, Object> model) {             // mapping to form
        model.put("authentication", authentication  );
        return "/jsp/login.jsp";
    }

    @GetMapping("/loginstatus")
    public @ResponseBody
    Authentication lstat(Authentication authentication, Map<String, Object> model) {
        return authentication;
    }


}
