'use strict';

import React from "react";
import client from "./../client";

import RecipeStep from "./RecipeStep";
import {MainContext} from "../context/MainContext";


class Recipe extends React.Component {

    constructor(props) {
        super(props);

        this.state ={
            recipeSteps: '',
            editMode: this.props.editMode,
            recipe: this.props.recipe,
            isShowEditButton: false
            ,shn: 'here'
        };



        this.handleSaveClick = this.handleSaveClick.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleDeleteRecipe = this.handleDeleteRecipe.bind(this);

        this.handleOnMouseEnterShow = this.handleOnMouseEnterShow.bind(this);
        this.handleOnMouseLeaveShow = this.handleOnMouseLeaveShow.bind(this);

        this.loadRecipe = this.loadRecipe.bind(this);
        this.loadRecipeSteps = this.loadRecipeSteps.bind(this);

        this.handleAddRecipeStep = this.handleAddRecipeStep.bind(this);

        this.handlerRefresh = this.handlerRefresh.bind(this);

    }

    handlerRefresh(e){
        console.log('HR Recipe ['+e+'] ');
        console.log(e);
        console.log(e.detail);
        console.log(this.state.recipe);
        this.loadRecipe();
    }

    componentDidMount() {

        // console.log('DID MOUNT ');
        // console.log(this.state.recipe);

        this.loadRecipeSteps();
        // this.loadRecipe();
        window.addEventListener('elRecipeReload',  this.handlerRefresh );

    }

    componentWillUnmount(){
        window.removeEventListener('elRecipeReload', this.handlerRefresh);
    }


    loadRecipe(){

        fetch(this.state.recipe._links.self.href)
            .then(response => response.json())
            .then(recipe => {
                this.setState({recipe: recipe});
                this.loadRecipeSteps();
            }) ;
    }

    loadRecipeSteps(){
        if (this.state.recipe.recipeStepsCount > 0){

            fetch(this.state.recipe._links.recipeSteps.href)
                .then(response => response.json())
                .then(response => {
                    let ret = response._embedded.recipeSteps.map(recipeStep => {
                        return (
                            <div key={recipeStep._links.self.href}>
                                <RecipeStep  recipeStep={recipeStep} isInAdmGroup={this.props.isInAdmGroup} loadRecipe={this.loadRecipe}/>
                                {this.props.isInAdmGroup && <button onClick={this.handleDeleteRecipeStep.bind(this,recipeStep._links.self.href)}>X</button>}
                            </div>
                        );
                    });
                    this.setState({ recipeSteps: ret });
                } );

        }else{
            let ret = (
                <p>NO recipe steps here </p>
            );
            this.setState({ recipeSteps: ret });
        }
    }

    handleDeleteRecipeStep(id){
        fetch(id,{
            method: 'DELETE'
        }).then(() => this.loadRecipe());

    }

    handleSaveClick() {

        fetch(this.props.recipe._links.self.href,{
            method: 'PUT',
            body: JSON.stringify(this.state.recipe),
            headers: {"Content-Type":"application/json"}
        }).then(()=>{
            this.props.handlerReloadRecipesList();
            this.setState({editMode: false});
            this.loadRecipe();
        });
    }

    handleDeleteRecipe(event){

        fetch(this.props.recipe._links.self.href,{
            method: 'DELETE'
        }).then(() => {
            this.props.handlerReloadRecipesList();
        });
    }

    handleEditClick() {
        this.setState({editMode: true});
    }


    handleChangeField(field,event){
        let tmpRecipe = this.state.recipe;
        if (field =='tts'){
            tmpRecipe.tts = event.target.value;
        }else if(field =='name'){
            tmpRecipe.name = event.target.value;
        }else if(field =='shortName'){
            tmpRecipe.shortName = event.target.value;
        }
        this.setState({recipe: tmpRecipe});
    }

    // recipe step
    handleAddRecipeStep(recipe){
        // console.log(recipe);
        // console.log(this.state.recipe);
        //new step
        fetch('/api/recipeSteps',{
            method: 'POST',
            body: JSON.stringify({recipeStep: {name: "TommyWTF", description: 'bla ['+new Date()+']'}}),
            headers: {"Content-Type":"application/json"}
        }).then(r => {
            // console.log(r);
            return r.json();
        }).then(r => {
            // console.log(r);
            // console.log(r._links.parentRecipe.href);
            // console.log(this.state.recipe._links.self.href);
            fetch(r._links.parentRecipe.href,{
                method: 'PUT',
                body: this.state.recipe._links.self.href,
                headers: {"Content-Type":"text/uri-list"}
            }).then(r => {
                // console.log('bind');
                // console.log(r);
                return r.text();
            }).then(r => {
                // console.log(r);
                //this.loadRecipeSteps();
                this.loadRecipe();
            });
        });
    }

    handleOnMouseEnterShow(event){
        this.setState({isShowEditButton: true})
    }

    handleOnMouseLeaveShow(event){
        this.setState({isShowEditButton: false})
    }

    // componentDidUpdate(){
        // console.log('Recipe componentDidUpdate');
        // console.log(this.state.recipe);
    // }

    render() {

        // console.log('Recipe RENDER');
        // console.log(this.state.recipe);
        this.state.shn = this.context.shn;

        const buttonEditRecipe = (
            <div>
                <span style={{fontSize: "18pt"}}> </span>{this.props.isInAdmGroup && this.state.isShowEditButton && <button onClick={this.handleEditClick} >Edit</button>}<span></span>
            </div>
        );



        const formEdit =
            <div style={{ borderStyle: "groove", padding1: "10px 5px", textAlign1: "right", color7: "blue", borderColor: "yellow" }} >

                <h5>Recepto HERE </h5>
                <p>Короткое название: <input type="text" value={this.state.recipe.shortName} onChange={this.handleChangeField.bind(this,'shortName')} /></p>
                <p>Название: <input type="text" value={this.state.recipe.name} onChange={this.handleChangeField.bind(this,'name')} /></p>
                <p>Озвучка: <input type="text" value={this.state.recipe.tts} onChange={this.handleChangeField.bind(this,'tts')} /></p>

                <p>По шагам: {this.state.recipe.recipeStepsCount} </p>

                <button onClick={this.handleSaveClick}>Save</button>

                <p><button onClick={this.handleAddRecipeStep.bind(this,this.state.recipe)}>AddRecipeStep</button></p>

                {this.state.recipeSteps}

            </div>;

        const formShow =
            <div style={{ borderStyle: "groove", padding1: "10px 5px", textAlign1: "right", color4: "blue" }} onMouseEnter={this.handleOnMouseEnterShow } onMouseLeave={this.handleOnMouseLeaveShow }>
                <h5>Recepto HERE {this.props.recipe._links.self.href} </h5>
                <p>{this.state.shn}</p>
                <p>{ this.props.isInAdmGroup && <button onClick={this.handleDeleteRecipe}>X</button> } </p>
                <p>Короткое название: {this.state.recipe.shortName}</p>
                <p>Название: {this.state.recipe.name}</p>
                <p>Озвучка: {this.state.recipe.tts}</p>
                <p>По шагам: {this.state.recipe.recipeStepsCount} </p>

                {buttonEditRecipe}
                {this.state.recipeSteps}
            </div>;

        if(this.state.editMode){
            return formEdit ;
        }else{
            return formShow;
        }

    };

}


Recipe.defaultProps = {isInAdmGroup: false, editMode: false, recipe: {shortName: "Tommy WTF def", name: "The Name def", tts: "the TTS default", recipeStepsCount: 0}};

export default Recipe