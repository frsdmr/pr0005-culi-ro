'use strict';

import React from "react";

import Recipe from "./Recipe";
//import client from "./../client";

import {MainContext} from './../context/MainContext';


class ListRecipes extends React.Component{


    constructor(props) {
        super(props);

        this.state={
            loadedListRecipes: [],
            isAddingState: false
        }

        this.handlerReloadRecipesList = this.handlerReloadRecipesList.bind(this);
        this.handlerAddButton = this.handlerAddButton.bind(this);

        this.handlerRefresh = this.handlerRefresh.bind(this);
    }


    handlerReloadRecipesList(recipe){
        //console.log(recipe);
        if(recipe == undefined){
            fetch('/api/recipes')
                .then(response => response.json())
                .then(response =>  {
                    //console.log(response._embedded.recipes);

                    this.setState({loadedListRecipes: response._embedded.recipes, isAddingState: false});
                    // this.context.mainRefresh('777 ['+new Date()+']');
                } );
        }else{
            let retRecipeArray =  [];
            retRecipeArray.push(recipe);
            this.setState({loadedListRecipes: retRecipeArray, isAddingState: true});
            // this.context.mainRefresh('777 ['+new Date()+']');

        }
    };

    handlerDeleteRecipe(recipe){
        //delete
        // console.log('delete by id '+id);
        fetch(recipe,{
            method: 'DELETE'
        }).then(() => this.handlerReloadRecipesList());
        //refresh(reload)

    };

    handlerAddButton(){

        let rcp = {shortName: "SHN["+new Date()+"]", name: "", tts: ""};

        fetch('/api/recipes',{
            method: 'POST',
            body: JSON.stringify(rcp),
            headers: {"Content-Type":"application/json"}
        })
            .then(r  => r.json() ) /// can read id here and reload only this doc in edit mode
            .then(r => {
                this.handlerReloadRecipesList(r);
            });

    }

    handlerRefresh(){
        console.log('HR listRecipes');
        // console.log(this.context);
        // this.context.shn = '88888888';//'SHN ['+new Date()+']';
        //this.context.mainRefresh();
        this.handlerReloadRecipesList();

        window.dispatchEvent(new CustomEvent('elRecipeReload',{detail:{abc: this.state.loadedListRecipes}}));
    }

    componentDidMount() {
        // let value = this.context;
        // console.log('***');
        // console.log(value);
        this.handlerReloadRecipesList();

        this.context.contextCallFromStepPlacedInLR = this.contextCallFromStepPlacedInLR;
        console.log(this.context);

    }
    contextCallFromStepPlacedInLR(xxx){
        console.log('contextCallFromStepPlacedInLR');
        console.log(this.context);
        console.log(xxx.recipeStep._links.self);
    }

    render(){

        const admGroup = '[thisAdmGroup/'+this.props.thisAdmGroup+']';
        const admGroupS = <span>[thisAdmGroup/ {this.props.thisAdmGroup ? 'T':'F'} ]</span>;

        let recipes = '';
        if(this.state.loadedListRecipes.length == 0){
            recipes = <div>No Recipes</div>;
        }else{
            recipes = this.state.loadedListRecipes.map(recipe => {
                return (
                    <div key={recipe._links.self.href}>
                        <Recipe isInAdmGroup={this.props.isInAdmGroup}  recipe={recipe} editMode={this.state.isAddingState}
                                handlerReloadRecipesList={this.handlerReloadRecipesList}/>
                        {this.props.isInAdmGroup && <button onClick = {this.handlerDeleteRecipe.bind(this,recipe._links.self.href)}    >XXX</button>}
                    </div>
                );
            });
        }

        return (
            <div style={{ borderStyle1: "groove"}}>
                {admGroup}
                {admGroupS}
                [thisAdmGroup/{this.props.thisAdmGroup ? 'T': 'F'}]
                [thisAdmGroup/{''+this.props.thisAdmGroup}]
                [{this.props.thisAdmGroup && 'Adm group'}]
                <br/>
                <button onClick={this.handlerAddButton}>add</button>
                <br/>
                {recipes}
                <br/>
                <hr/>
                <button onClick={this.handlerRefresh}>***</button>
                <br/>
            </div>
        );

    };

}

ListRecipes.contextType = MainContext;

ListRecipes.defaultProps = {isInAdmGroup: false};

export default ListRecipes