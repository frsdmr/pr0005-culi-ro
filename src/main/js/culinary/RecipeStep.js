'use strict';

import React from 'react';
import {MainContext} from "../context/MainContext";


class RecipeStep extends React.Component {

    constructor(props) {
        super(props);

        this.handleDeleteRecipeStep = this.handleDeleteRecipeStep.bind(this);
    }

    componentDidMount(){

        console.log(this.context);
    }

    handleDeleteRecipeStep(){

        //console.log('delete step inside');

        fetch(this.props.recipeStep._links.self.href,{
            method: 'DELETE'
        }).then(() => { this.props.loadRecipe() });

    }


    render(){

        const buttonDeleteRecipeStep =  this.props.isInAdmGroup && <button onClick={this.handleDeleteRecipeStep}>X </button>;


        return (
            <div style={{ borderStyle: "groove", padding1: "10px 5px", textAlign1: "center", color: "green"}}>
                <p>Имя: {this.props.recipeStep.name}</p>
                <p>Описание: {this.props.recipeStep.description}</p>
                {buttonDeleteRecipeStep}
                <button onClick={this.context.contextCallPlacedInApp.bind(this,this.props)}>App</button>
                <button onClick={this.context.contextCallFromStepPlacedInLR.bind(this,this.props)}>LR</button>
            </div>
        );
    };
}

RecipeStep.contextType = MainContext;

RecipeStep.defaultProps = {recipeStep: {name: "TommyWTF", description: 22}};

export default RecipeStep