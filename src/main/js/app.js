'use strict';

// import Promise from 'promise-polyfill';
// var Promise = require('es6-promise').Promise;

// import Promise from "es6-promise-promise";
require('es6-promise-promise');     // for IE-support (it work with package.json with dep ("es6-promise-promise": "^1.0.0",))

// import {fetch as fetchForIE} from "whatwg-fetch";   // for IE-support
require('whatwg-fetch');

// let fetch;
// if (window.fetch){
//     fetch = window.fetch;
//     console.log('--');
// }else{
//     fetch = fetchForIE;
//     console.log('ie');
// }



import React from "react";
import ReactDOM from "react-dom";




import ListRecipes from "./culinary/ListRecipes";

import {MainContext} from './context/MainContext';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = { data: '', isLoading: false, isInAdmGroup: false, contData: {aaa:'bbb',cc:'dd'}};

        // this.handlerRefresh = this.handlerRefresh.bind(this);
    }


    componentDidMount() {

        fetch('/loginstatus')
            .then(response =>   response.json()    )
            .then(response => {

                let inAdmGroup = false;
                response.principal.authorities.map(elem => {
                    if (!!([ 'ROLE_ADM'/*, 'ROLE_MGROUP'*/].indexOf(elem.authority)+1)) inAdmGroup  = true;
                    // if(inAdmGroup){
                    //     fetch('/api/recipes')
                    //         .then(response => response.json())
                    //         .then(response => this.setState({loadedListRecipes: response._embedded.recipes}) );
                    // }
                });
                this.setState({isInAdmGroup: inAdmGroup});

            });



        fetch('/yandex_4f2d534486f17935.html')
            .then(response => {
                this.setState({ isLoading: (response.status == 200) })
                return   response.text();
            }    )
            .then(data => {
                let xx = new DOMParser()
                    .parseFromString(data, 'text/html').body
                    .firstChild.textContent;
                //this.setState({ data: xx });
                this.setState({ data: (this.state.isLoading? xx:'###') });
                //console.log(JSON.stringify({ name: 'Hubot', login: 'hubot', }));
            });

        // let temp = this.state.contData; //{shn: 'SHN '+ new Date()};
        // temp.aaa = 'eee';
        // temp.handlerRefresh = this.handlerRefresh;
        // console.log('*');
        // console.log(temp);
        // this.setState({contData: temp});
        //this.context.contextCallFromStep1 = this.contextCallFromStep1;
        this.state.contData.shn = 'SHN HERE 777 (11:24)';
        this.state.contData.contextCallPlacedInApp = this.contextCallPlacedInApp;
        console.log(this.state.contData);
    }

    contextCallPlacedInApp(xxx){
        console.log('contextCallPlacedInApp THE APP');
        console.log(xxx.recipeStep._links.self);
    }


    // handlerRefresh(eee){
    //     console.log('refr app ['+eee+']');
    // }

    render() {
        return (
            <MainContext.Provider value={this.state.contData}>
                <div>
                    <h4>SHNXa [{this.state.data}] {this.state.contData.aaa} </h4>
                    <ListRecipes  isInAdmGroup = {this.state.isInAdmGroup} />
                </div>
            </MainContext.Provider>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('react')
);


