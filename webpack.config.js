var path = require('path');
// var webpack = require("webpack");

module.exports = {
    entry: './src/main/js/app.js',
    devtool: 'sourcemaps',
    cache: true,
    mode: 'development',
    output: {
        path: __dirname,
        filename: './src/main/webapp/built/bundle.js'       // modifying here (instead no-copying DevToolsSettings(./src/main/resources/__static__/built/bundle.js) )
    },
    // plugins: [
    //     //new webpack.ProvidePlugin({Fetch: 'whatwg-fetch'}),             // IE support fetch _don't work_ here (need use a   require('whatwg-fetch');   )
    //     new webpack.ProvidePlugin({Promise: 'es6-promise-promise'})     // IE support for promise it can used (but i think it's a bad idea use 2 places(this file and app.js) for config it...)
    // ],
    module: {
        rules: [
            {
                test: path.join(__dirname, '.'),
                exclude: /(node_modules)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"]
                    }
                }]
            }
        ]
    }
};